package org.mpierce.demo.java8;

import org.junit.Test;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public final class NashornTest {

    private ScriptEngine engine = new ScriptEngineManager().getEngineByName("Nashorn");

    @Test
    public void testHelloWorld() throws ScriptException {
        System.out.println(engine.eval("'Hello, world!'"));
    }

    @Test
    public void testLoadScript() throws ScriptException {
        engine.eval(new InputStreamReader(getClass().getResourceAsStream("factorial.js")));
        assertEquals(24.0, (double) engine.eval("factorial(4)"), 0.00001);
    }

    @Test
    public void testCallJavaFromJs() throws ScriptException {
        engine.eval(new InputStreamReader(getClass().getResourceAsStream("java-from-js.js")));
        Bindings bindings = engine.getContext().getBindings(ScriptContext.ENGINE_SCOPE);
        assertEquals(DayOfWeek.THURSDAY, ((LocalDate) bindings.get("nextThursday")).getDayOfWeek());
    }
}
