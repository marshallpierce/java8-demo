package org.mpierce.demo.java8;

import org.junit.Test;

public final class RunnableTest {
    @Test
    public void testWithAnonymousInnerClass() {
        doIt(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello world");
            }
        });
    }

    @Test
    public void testWithAnonymousInnerClassClosure() {
        String name = "world";
        doIt(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello " + name);
            }
        });
    }

    @Test
    public void testWithLambda() {
        doIt(() -> System.out.println("Hello world"));
    }

    @Test
    public void testWithLambdaClosure() {
        String name = "world";
        doIt(() -> System.out.println("Hello " + name));
    }

    @Test
    public void testWithLambdaAssignment() {
        Runnable r = () -> System.out.println("Hello world");
        doIt(r);
    }

    static void doIt(Runnable r) {
        r.run();
    }
}
