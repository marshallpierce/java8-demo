package org.mpierce.demo.java8;

import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public final class MethodRefTest {

    @Test
    public void testClassStaticMethodRef() {
        assertEquals(4, calculateLength("abcd", MethodRefTest::staticCalculator));
    }

    @Test
    public void testClassInstanceMethodRef() {
        assertEquals(4, calculateLength("abcd", String::length));
    }

    @Test
    public void testObjectInstanceMethodRef() {
        StringLengthCalculator calculator = new StringLengthCalculator();
        assertEquals(4, calculateLength("abcd", calculator::tellMeTheLength));
    }

    static int calculateLength(String s, Function<String, Integer> lengthCalculator) {
        return lengthCalculator.apply(s);
    }

    static int staticCalculator(String s) {
        return s.length();
    }

    static class StringLengthCalculator {
        int tellMeTheLength(String s) {
            return s.length();
        }
    }
}
