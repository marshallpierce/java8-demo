package org.mpierce.demo.java8;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public final class StreamTest {

    @Test
    public void testIterationCount() {
        int hasS = 0;
        for (String s : words()) {
            if (s.contains("s")) {
                hasS++;
            }
        }

        assertEquals(3, hasS);
    }

    @Test
    public void testStreamFilterCount() {
        long hasS = words().stream()
            .filter((s) -> s.contains("s"))
            .count();
        assertEquals(3, hasS);
    }

    @Test
    public void testStreamSkip() {
        assertEquals(Optional.of("some"), words().stream()
            .skip(2)
            .findFirst());
    }

    @Test
    public void testSortAndLimit() {
        assertEquals("are", words().stream()
            .sorted()
            .findFirst()
            .get());
    }

    @Test
    public void testTotalLengthReduce() {
        assertEquals(17, words().stream()
            .reduce("", (s1, s2) -> s1 + s2)
            .length());
    }

    @Test
    public void testTotalLengthMapReduce() {
        assertEquals(17, words().stream()
            .map(String::length)
            .reduce(0, Integer::sum)
            .intValue());
    }

    @Test
    public void testTotalLengthMapReduceIntStream() {
        assertEquals(17, words().stream()
            .mapToInt(String::length)
            .reduce(0, Integer::sum));
    }

    @Test
    public void testTotalLengthMapReduceIntStreamOptional() {
        assertEquals(17, words().stream()
            .mapToInt(String::length)
            .reduce(Integer::sum)
            .orElse(-1));
    }

    @Test
    public void testTotalLengthMapReduceIntStreamSum() {
        assertEquals(17, words().stream()
            .mapToInt(String::length)
            .sum());
    }

    @Test
    public void testLetterCounts() {
        // explicit <String> is to work around a bug in IntelliJ; not actually needed by compiler
        Map<String, Integer> letterCounts = words().stream()
            .flatMap((s) -> Pattern.compile("").splitAsStream(s))
            .collect(Collectors.toMap(Function.<String>identity(), (s) -> 1, Integer::sum));
        assertEquals(4, letterCounts.get("e").intValue());
    }

    private static List<String> words() {
        return Arrays.asList("these", "are", "some", "words");
    }
}
