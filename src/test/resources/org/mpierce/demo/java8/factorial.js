var factorial = function(i) {
    if (i == 1 || i == 0) {
        return 1;
    }

    return i * factorial(i - 1);
};