var LocalDate = java.time.LocalDate;
var TemporalAdjusters = Java.type('java.time.temporal.TemporalAdjusters');

var today = LocalDate.now();

var nextThursday = today.with(TemporalAdjusters.next(java.time.DayOfWeek.THURSDAY));